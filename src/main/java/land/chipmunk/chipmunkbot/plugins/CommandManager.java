package land.chipmunk.chipmunkbot.plugins;

import com.mojang.brigadier.builder.ArgumentBuilder;
import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.command.CommandSource;
import land.chipmunk.chipmunkbot.command.ComponentMessage;
import land.chipmunk.chipmunkbot.command.BuiltInExceptions;
import land.chipmunk.chipmunkbot.commands.*;
import lombok.RequiredArgsConstructor;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.format.TextDecoration;
import net.kyori.adventure.text.event.ClickEvent;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.tree.RootCommandNode;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.Message;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import lombok.Getter;
import lombok.Setter;

public class CommandManager {
  private ChipmunkBot client;
  @Getter @Setter private CommandDispatcher<CommandSource> dispatcher = new CommandDispatcher<>();

  static {
    // ? Is messing with static properties a good idea?
    CommandSyntaxException.BUILT_IN_EXCEPTIONS = new BuiltInExceptions();
  }

  public CommandManager (ChipmunkBot client) {
    this.client = client;

    TestCommand.register(dispatcher);
    HelpCommand.register(dispatcher);
    RunCommand.register(dispatcher);
    EchoCommand.register(dispatcher);
    InfoCommand.register(dispatcher);
    ReconnectCommand.register(dispatcher);
    NetMsgCommand.register(dispatcher);
    MusicCommand.register(dispatcher);
    LogQueryCommand.register(dispatcher);
    SayCommand.register(dispatcher);
  }

  public static void sendException (CommandSource source, CommandSyntaxException exception) {
    final Message message = exception.getRawMessage();
    Component component;
    if (message instanceof ComponentMessage) component = ((ComponentMessage) message).component();
    else component = Component.text(message.getString());

    source.sendOutput(component.color(NamedTextColor.RED), false);

    final Component context = getContext(exception);
    if (context != null) source.sendOutput(context, false);
  }

  public static Component getContext (CommandSyntaxException exception) {
    final int _cursor = exception.getCursor();
    final String input = exception.getInput();

    if (input == null || _cursor < 0) {
      return null;
    }
    Component component = Component.empty()
      .color(NamedTextColor.GRAY)
      .clickEvent(ClickEvent.suggestCommand(input));

    final int cursor = Math.min(input.length(), _cursor);

    if (cursor > CommandSyntaxException.CONTEXT_AMOUNT) {
      component = component.append(Component.text("..."));
    }

    component = component
      .append(Component.text(input.substring(Math.max(0, cursor - CommandSyntaxException.CONTEXT_AMOUNT), cursor)))
      .append(Component.text(input.substring(cursor), NamedTextColor.RED).decoration(TextDecoration.UNDERLINED, true))
      .append(Component.translatable("command.context.here", NamedTextColor.RED).decoration(TextDecoration.ITALIC, true));

    return Component.empty().color(NamedTextColor.RED).append(component); // ? Should I Impostor Notchian here?
  }

  public static LiteralArgumentBuilder<CommandSource> literal(String name) { return LiteralArgumentBuilder.<CommandSource>literal(name); }
  public static <T> RequiredArgumentBuilder<CommandSource, T> argument(String name, ArgumentType<T> type) { return RequiredArgumentBuilder.<CommandSource, T>argument(name, type); }
}
