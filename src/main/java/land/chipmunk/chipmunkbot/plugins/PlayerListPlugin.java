package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import org.geysermc.mcprotocollib.protocol.packet.ingame.clientbound.ClientboundPlayerInfoUpdatePacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.clientbound.ClientboundPlayerInfoRemovePacket;
import org.geysermc.mcprotocollib.network.packet.Packet;
import org.geysermc.mcprotocollib.network.Session;
import org.geysermc.mcprotocollib.network.event.session.SessionAdapter;
import org.geysermc.mcprotocollib.network.event.session.SessionListener;
import org.geysermc.mcprotocollib.protocol.data.game.PlayerListEntry;
import org.geysermc.mcprotocollib.protocol.data.game.PlayerListEntryAction;
import net.kyori.adventure.text.Component;
import land.chipmunk.chipmunkbot.data.MutablePlayerListEntry;

import java.util.List;
import java.util.ArrayList;
import java.util.UUID;

public class PlayerListPlugin extends SessionAdapter {
  private ChipmunkBot client;
  public List<MutablePlayerListEntry> list = new ArrayList<>();

  public PlayerListPlugin (ChipmunkBot client) {
    this.client = client;
    client.addListener((SessionListener) this);
  }

  @Override
  public void packetReceived (Session session, Packet packet) {
    if (packet instanceof ClientboundPlayerInfoUpdatePacket) packetReceived(session, (ClientboundPlayerInfoUpdatePacket) packet);
    else if (packet instanceof ClientboundPlayerInfoRemovePacket) packetReceived(session, (ClientboundPlayerInfoRemovePacket) packet);
  }

  public void packetReceived (Session session, ClientboundPlayerInfoUpdatePacket packet) {
    for (PlayerListEntryAction action : packet.getActions()) {
      for (PlayerListEntry entry : packet.getEntries()) {
        if (action == PlayerListEntryAction.ADD_PLAYER) addPlayer(entry);
        else if (action == PlayerListEntryAction.INITIALIZE_CHAT) initializeChat(entry);
        else if (action == PlayerListEntryAction.UPDATE_GAME_MODE) updateGamemode(entry);
        else if (action == PlayerListEntryAction.UPDATE_LISTED) updateListed(entry);
        else if (action == PlayerListEntryAction.UPDATE_LATENCY) updateLatency(entry);
        else if (action == PlayerListEntryAction.UPDATE_DISPLAY_NAME) updateDisplayName(entry);
      }
    }
  }

  public void packetReceived (Session session, ClientboundPlayerInfoRemovePacket packet) {
    for (UUID uuid : packet.getProfileIds()) {
      removePlayer(uuid);
    }
  }

  public final MutablePlayerListEntry getEntry (UUID uuid) {
    for (MutablePlayerListEntry candidate : list) {
      if (candidate.profile().getId().equals(uuid)) {
        return candidate;
      }
    }

    return null;
  }

  public final MutablePlayerListEntry getEntry (String username) {
    for (MutablePlayerListEntry candidate : list) {
      if (candidate.profile().getName().equals(username)) {
        return candidate;
      }
    }

    return null;
  }

  public final MutablePlayerListEntry getEntry (Component displayName) {
    for (MutablePlayerListEntry candidate : list) {
      if (candidate.displayName() != null && candidate.displayName().equals(displayName)) {
        return candidate;
      }
    }

    return null;
  }

  private final MutablePlayerListEntry getEntry (PlayerListEntry other) {
    return getEntry(other.getProfileId());
  }

  private void addPlayer (PlayerListEntry newEntry) {
    final MutablePlayerListEntry duplicate = getEntry(newEntry);
    if (duplicate != null) list.remove(duplicate);

    list.add(new MutablePlayerListEntry(newEntry));
  }

  private void initializeChat (PlayerListEntry newEntry) {
    final MutablePlayerListEntry target = getEntry(newEntry);
    if (target == null) return;

    target.publicKey(newEntry.getPublicKey());
  }

  private void updateGamemode (PlayerListEntry newEntry) {
    final MutablePlayerListEntry target = getEntry(newEntry);
    if (target == null) return;

    target.gamemode(newEntry.getGameMode());
  }

  private void updateListed (PlayerListEntry newEntry) {
    final MutablePlayerListEntry target = getEntry(newEntry);
    if (target == null) return;

    target.listed(newEntry.isListed());
  }

  private void updateLatency (PlayerListEntry newEntry) {
    final MutablePlayerListEntry target = getEntry(newEntry);
    if (target == null) return;

    target.latency(newEntry.getLatency());
  }

  private void updateDisplayName (PlayerListEntry newEntry) {
    final MutablePlayerListEntry target = getEntry(newEntry);
    if (target == null) return;

    target.displayName(newEntry.getDisplayName());
  }

  private void removePlayer (UUID uuid) {
    final MutablePlayerListEntry target = getEntry(uuid);
    if (target == null) return;

    client.tabComplete().complete("/scoreboard players add ").thenApply(packet -> {
      final String[] matches = packet.getMatches();
      final Component[] tooltips = packet.getTooltips();
      final String username = target.profile().getName();

      for (int i = 0; i < matches.length; i++) {
        if (tooltips[i] != null || !matches[i].equals(username)) continue;
        target.listed(false);
        return packet;
      }

      list.remove(target);
      return packet;
    });
  }
}
