package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.Configuration;
import land.chipmunk.chipmunkbot.data.BlockArea;
import org.cloudburstmc.math.vector.Vector3i;
import org.cloudburstmc.nbt.NbtMap;
import org.cloudburstmc.nbt.NbtMapBuilder;
import org.geysermc.mcprotocollib.network.packet.Packet;
import org.geysermc.mcprotocollib.network.Session;
import org.geysermc.mcprotocollib.network.event.session.SessionListener;
import org.geysermc.mcprotocollib.network.event.session.SessionAdapter;
import org.geysermc.mcprotocollib.network.event.session.DisconnectedEvent;
import org.geysermc.mcprotocollib.protocol.data.game.item.ItemStack;
import org.geysermc.mcprotocollib.protocol.data.game.item.component.DataComponent;
import org.geysermc.mcprotocollib.protocol.data.game.item.component.DataComponentType;
import org.geysermc.mcprotocollib.protocol.data.game.item.component.DataComponents;
import org.geysermc.mcprotocollib.protocol.packet.ingame.clientbound.entity.player.ClientboundPlayerPositionPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.serverbound.player.ServerboundUseItemOnPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.serverbound.inventory.ServerboundSetCreativeModeSlotPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.serverbound.player.ServerboundPlayerActionPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.serverbound.inventory.ServerboundSetCommandBlockPacket;
import org.geysermc.mcprotocollib.protocol.data.game.entity.player.Hand;
import org.geysermc.mcprotocollib.protocol.data.game.entity.object.Direction;
import org.geysermc.mcprotocollib.protocol.data.game.entity.player.PlayerAction;
import org.geysermc.mcprotocollib.protocol.data.game.level.block.CommandBlockMode;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.concurrent.CompletableFuture;

public class CommandCore extends SessionAdapter {
  private ChipmunkBot client;
  @Getter @Setter private boolean enabled = false;
  @Getter @Setter private boolean ready = false; 
  @Getter @Setter private Vector3i origin;
  // TODO: Make it configurable
  @Getter private final BlockArea relativeArea = new BlockArea(Vector3i.from(0, 0, 0), Vector3i.from(15, 0, 15));
  @Getter @Setter private Vector3i currentBlockRelative;
  private Timer refillTimer;

  @Getter private List<Listener> listeners = new ArrayList<>();

  public CommandCore (ChipmunkBot client, Configuration.Bot options) {
    this.client = client;
    this.enabled = options.core.enabled;
    client.addListener((SessionListener) this);
  }

  @Override
  public void packetReceived (Session session, Packet packet) {
    if (packet instanceof ClientboundPlayerPositionPacket) packetReceived(session, (ClientboundPlayerPositionPacket) packet);
  }

  public void packetReceived (Session session, ClientboundPlayerPositionPacket packet) {
    if (!ready) {
      ready = true;

      if (enabled) {
        final TimerTask refillTask = new TimerTask () { @Override public void run () { refill(); } };
        refillTimer = new Timer();
        refillTimer.schedule(refillTask, 60L * 1000L, 60L * 1000L);
      }

      for (Listener listener : listeners) listener.ready();
    }

    if (!enabled) return;

    final Vector3i oldOrigin = origin;
    origin = Vector3i.from(
      ((int) packet.getX() / 16) * 16,
      0, // TODO: Use the actual bottom of the world instead of hardcoding to 0
      ((int) packet.getZ() / 16) * 16
    );

    if (currentBlockRelative == null) currentBlockRelative = Vector3i.from(relativeArea.start());
    if (!origin.equals(oldOrigin)) refill();
  }

  public void refill () {
    // final PositionManager position = client.position();
    final Vector3i relStart = relativeArea.start();
    final Vector3i relEnd = relativeArea.end();

    final String command = String.format(
      "fill %s %s %s %s %s %s minecraft:command_block",
      relStart.getX() + origin.getX(),
      relStart.getY() + origin.getY(),
      relStart.getZ() + origin.getZ(),

      relEnd.getX() + origin.getX(),
      relEnd.getY() + origin.getY(),
      relEnd.getZ() + origin.getZ()
    );

    final NbtMapBuilder blockEntityTagBuilder = NbtMap.builder();

    blockEntityTagBuilder.putString("id", "minecraft:command_block");
    blockEntityTagBuilder.putString("Command", command);
    blockEntityTagBuilder.putByte("auto", (byte) 1);

    final NbtMap blockEntityTag = blockEntityTagBuilder.build();

    final Map<DataComponentType<?>, DataComponent<?, ?>> map = new HashMap<>();

    map.put(DataComponentType.BLOCK_ENTITY_DATA, DataComponentType.BLOCK_ENTITY_DATA.getDataComponentFactory().create(DataComponentType.BLOCK_ENTITY_DATA, blockEntityTag));

    final DataComponents dataComponents = new DataComponents(map);

    final PositionManager position = client.position();

    Vector3i temporaryBlockPosition = Vector3i.from((int) position.x(), (int) position.y() - 1, (int) position.z());

    // client.chat().command(command);

    final Session session = client.session();
    session.send(new ServerboundSetCreativeModeSlotPacket((short) 45, new ItemStack(395 /* command_block */, 1, dataComponents)));
    session.send(new ServerboundPlayerActionPacket(PlayerAction.START_DIGGING, temporaryBlockPosition, Direction.NORTH, 0));
    session.send(new ServerboundUseItemOnPacket(temporaryBlockPosition, Direction.NORTH, Hand.OFF_HAND, 0.5f, 0.5f, 0.5f, false, 0));
  }

  public int maxCommandLength () {
    if (!enabled) return 256;
    return 32767;
  }

  public void incrementCurrentBlock () {
    final Vector3i start = relativeArea.start();
    final Vector3i end = relativeArea.end();

    int x = currentBlockRelative.getX();
    int y = currentBlockRelative.getY();
    int z = currentBlockRelative.getZ();

    x++;

    if (x > end.getX()) {
      x = start.getX();
      z++;
    }

    if (z > end.getZ()) {
      z = start.getZ();
      y++;
    }

    if (y > end.getY()) {
      x = start.getX();
      y = start.getY();
      z = start.getZ();
    }

    currentBlockRelative = Vector3i.from(x, y, z);
  }

  public Vector3i currentBlockAbsolute () {
    return currentBlockRelative.add(origin);
  }

  public void run (String command) {
    if (command.length() > maxCommandLength()) return;

    if (!enabled) {
      client.chat().command(command); // fall back to chat
      return;
    }

    final Session session = client.session();
    final Vector3i currentBlock = currentBlockAbsolute();

    // TODO: Support using repeating command blocks (on kaboom-like servers) (because less packets)
    session.send(new ServerboundSetCommandBlockPacket(currentBlock, "", CommandBlockMode.REDSTONE, false, false, false));
    session.send(new ServerboundSetCommandBlockPacket(currentBlock, command, CommandBlockMode.REDSTONE, false, false, true));

    incrementCurrentBlock();
  }

  public CompletableFuture<NbtMap> runTracked (String command) {
    if (command.length() > maxCommandLength()) return emptyNbtMapFuture();

    if (!enabled) {
      client.chat().command(command); // fall back to chat

      return emptyNbtMapFuture();
    }

    final Session session = client.session();
    final Vector3i currentBlock = currentBlockAbsolute();

    // TODO: Support using repeating command blocks (on kaboom-like servers) (because less packets)
    session.send(new ServerboundSetCommandBlockPacket(currentBlock, "", CommandBlockMode.SEQUENCE, false, false, false));
    session.send(new ServerboundSetCommandBlockPacket(currentBlock, command, CommandBlockMode.REDSTONE, true, false, true));

    incrementCurrentBlock();

    CompletableFuture<NbtMap> future = new CompletableFuture<NbtMap>();

    final Timer timer = new Timer();
    final TimerTask queryTask = new TimerTask() {
      public void run () {
        client.query().block(currentBlock)
          .thenApply(tag -> { future.complete(tag); return tag; });

        timer.purge();
      }
    };

    timer.schedule(queryTask, 50);

    return future;
  }

  private CompletableFuture<NbtMap> emptyNbtMapFuture () {
    CompletableFuture<NbtMap> future = new CompletableFuture<NbtMap>();
    future.complete(NbtMap.EMPTY);
    return future;
  }

  @Override
  public void disconnected (DisconnectedEvent event) {
    origin = null;
    currentBlockRelative = null;
    ready = false;

    if (refillTimer != null) {
      refillTimer.cancel();
      refillTimer.purge();
      refillTimer = null;
    }
  }

  public static class Listener {
    public void ready () {}
  }

  public void addListener (Listener listener) { listeners.add(listener); }
}
