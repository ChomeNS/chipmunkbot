package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import org.geysermc.mcprotocollib.network.packet.Packet;
import org.geysermc.mcprotocollib.network.Session;
import org.geysermc.mcprotocollib.network.packet.PacketProtocol;
import org.geysermc.mcprotocollib.protocol.MinecraftProtocol;
import org.geysermc.mcprotocollib.protocol.data.game.entity.player.GameMode;
import org.geysermc.mcprotocollib.protocol.data.ProtocolState;
import org.geysermc.mcprotocollib.protocol.data.game.level.notify.GameEvent;
import org.geysermc.mcprotocollib.protocol.data.game.level.notify.GameEventValue;
import org.geysermc.mcprotocollib.protocol.data.game.entity.EntityEvent;
import org.geysermc.mcprotocollib.protocol.data.game.ClientCommand;
import org.geysermc.mcprotocollib.network.event.session.SessionAdapter;
import org.geysermc.mcprotocollib.network.event.session.SessionListener;
import org.geysermc.mcprotocollib.network.event.session.DisconnectedEvent;
import org.geysermc.mcprotocollib.protocol.packet.ingame.clientbound.ClientboundLoginPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.clientbound.level.ClientboundGameEventPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.clientbound.entity.ClientboundEntityEventPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.serverbound.ServerboundClientCommandPacket;
import land.chipmunk.chipmunkbot.Configuration;
import land.chipmunk.chipmunkbot.util.ComponentUtilities;
import lombok.Getter;
import lombok.Setter;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;

import java.util.Timer;
import java.util.TimerTask;

public class SelfCarePlugin extends SessionAdapter {
  private final ChipmunkBot client;
  private final Configuration.Bot options;

  @Getter @Setter private GameMode gamemode;
  @Getter @Setter private int permissionLevel;

  private boolean prefix = false;

  private Timer timer;
  private int entityId; // TODO: Move entity id handling somewhere else

  public SelfCarePlugin (ChipmunkBot client, Configuration.Bot options) {
    this.client = client;
    this.options = options;
    client.addListener((SessionListener) this);

    client.chat().addListener(new ChatPlugin.Listener() {
      @Override
      public void systemMessageReceived(Component component, boolean overlay) {
        // mabe parse using components? too lazy
        final String stringifiedMessage = ComponentUtilities.stringify(component);

        if (stringifiedMessage.equals("You now have the tag: " + options.selfCares.prefix.prefix)) prefix = true;
        else if (stringifiedMessage.equals("You no longer have a tag")) prefix = false;
        else if (stringifiedMessage.startsWith("You now have the tag: ")) prefix = false;
      }
    });

    final TimerTask task = new TimerTask() {
      @Override
      public void run () {
        final Session session = client.session();
        final PacketProtocol protocol = session.getPacketProtocol();
        if (!session.isConnected() || (protocol instanceof MinecraftProtocol && ((MinecraftProtocol) protocol).getOutboundState() != ProtocolState.GAME)) return;

        // TODO: Maybe make it possible for other stuff to listen for ticks
        tick();
      }
    };

    timer = new Timer();
    timer.schedule(task, 125, 125);
  }

  public void tick () {
    if (permissionLevel < 2) client.chat().command("minecraft:op @s[type=player]");
    else if (gamemode != GameMode.CREATIVE) client.chat().command("minecraft:gamemode creative");
    else if (!client.commandSpy().enabled() && options.selfCares.cspy) client.chat().command("c on");
    else if (!prefix && options.selfCares.prefix.enabled) client.chat().command("prefix " + options.selfCares.prefix.prefix);
  }

  @Override
  public void packetReceived (Session session, Packet packet) {
    if (packet instanceof ClientboundGameEventPacket) packetReceived(session, (ClientboundGameEventPacket) packet);
    else if (packet instanceof ClientboundEntityEventPacket) packetReceived(session, (ClientboundEntityEventPacket) packet);
    else if (packet instanceof ClientboundLoginPacket) packetReceived(session, (ClientboundLoginPacket) packet);
  }

  public void packetReceived (Session session, ClientboundGameEventPacket packet) {
    final GameEvent notification = packet.getNotification();
    final GameEventValue value = packet.getValue();

    if (notification == GameEvent.CHANGE_GAMEMODE) gamemode = (GameMode) value;
    else if (notification == GameEvent.ENTER_CREDITS) session.send(new ServerboundClientCommandPacket(ClientCommand.RESPAWN));
  }

  public void packetReceived (Session session, ClientboundLoginPacket packet) {
    entityId = packet.getEntityId(); // TODO: Move entity id handling somewhere else
    gamemode = packet.getCommonPlayerSpawnInfo().getGameMode();
  }

  public void packetReceived (Session session, ClientboundEntityEventPacket packet) {
    if (packet.getEntityId() != entityId) return;
    final EntityEvent event = packet.getEvent();

    if (event == EntityEvent.PLAYER_OP_PERMISSION_LEVEL_0) permissionLevel = 0;
    else if (event == EntityEvent.PLAYER_OP_PERMISSION_LEVEL_1) permissionLevel = 1;
    else if (event == EntityEvent.PLAYER_OP_PERMISSION_LEVEL_2) permissionLevel = 2;
    else if (event == EntityEvent.PLAYER_OP_PERMISSION_LEVEL_3) permissionLevel = 3;
    else if (event == EntityEvent.PLAYER_OP_PERMISSION_LEVEL_4) permissionLevel = 4;
  }

  @Override
  public void disconnected (DisconnectedEvent event) {
    if (client.reconnectDelay() >= 0) return;
    timer.cancel();
    timer.purge();
  }
}
