package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.data.chat.PlayerMessage;
import land.chipmunk.chipmunkbot.data.chat.SystemChatParser;
import land.chipmunk.chipmunkbot.data.MutablePlayerListEntry;
import land.chipmunk.chipmunkbot.util.UUIDUtilities;
import org.cloudburstmc.nbt.NbtMap;
import org.cloudburstmc.nbt.NbtMapBuilder;
import org.geysermc.mcprotocollib.protocol.data.game.item.ItemStack;
import org.geysermc.mcprotocollib.protocol.data.game.item.component.DataComponent;
import org.geysermc.mcprotocollib.protocol.data.game.item.component.DataComponentType;
import org.geysermc.mcprotocollib.protocol.data.game.item.component.DataComponents;
import org.geysermc.mcprotocollib.protocol.packet.ingame.clientbound.ClientboundSystemChatPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.clientbound.ClientboundPlayerChatPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.clientbound.ClientboundDisguisedChatPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.serverbound.ServerboundChatPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.serverbound.ServerboundChatCommandPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.serverbound.inventory.ServerboundSetCreativeModeSlotPacket;
import org.geysermc.mcprotocollib.network.packet.Packet;
import org.geysermc.mcprotocollib.network.Session;
import org.geysermc.mcprotocollib.network.event.session.SessionAdapter;
import org.geysermc.mcprotocollib.network.event.session.SessionListener;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import lombok.Getter;

import java.util.*;
import java.time.Instant;
import java.nio.charset.StandardCharsets;

import land.chipmunk.chipmunkbot.systemChat.*;

public class ChatPlugin extends SessionAdapter {
  private final ChipmunkBot client;
  @Getter private List<Listener> listeners = new ArrayList<>();

  private List<SystemChatParser> systemChatParsers;

  public ChatPlugin (ChipmunkBot client) {
    this.client = client;
    client.addListener((SessionListener) this);

    systemChatParsers = new ArrayList<>();
    systemChatParsers.add(new MinecraftChatParser(client));
    systemChatParsers.add(new KaboomChatParser(client));
    systemChatParsers.add(new U203aChatParser(client));
  }

  public void message (String message) {
    if (message.length() > 256) return;

    final ServerboundChatPacket packet = new ServerboundChatPacket(message, Instant.now().toEpochMilli(), 0, null, 0, new BitSet());
    client.session().send(packet);
  }

  public void command (String command) {
    if (command.length() > 256) return;

    final ServerboundChatCommandPacket packet = new ServerboundChatCommandPacket(command);
    client.session().send(packet);
  }

  @Override
  public void packetReceived (Session session, Packet packet) {
    if (packet instanceof ClientboundSystemChatPacket) packetReceived(session, (ClientboundSystemChatPacket) packet);
    else if (packet instanceof ClientboundPlayerChatPacket) packetReceived(session, (ClientboundPlayerChatPacket) packet);
    else if (packet instanceof ClientboundDisguisedChatPacket) packetReceived(session, (ClientboundDisguisedChatPacket) packet);
  }

  public void packetReceived (Session session, ClientboundSystemChatPacket packet) {
    final Component component = packet.getContent();
    final boolean overlay = packet.isOverlay();

    PlayerMessage playerMessage = null;

    for (SystemChatParser parser : systemChatParsers) {
      playerMessage = parser.parse(component);
      if (playerMessage != null) break;
    }

    for (Listener listener : this.listeners) {
      listener.systemMessageReceived(component, overlay);
      if (playerMessage != null) listener.playerMessageReceived(playerMessage);
    }
  }

  public void packetReceived (Session session, ClientboundPlayerChatPacket packet) {
    final MutablePlayerListEntry sender = client.playerList().getEntry(packet.getSender());
    if (sender == null) return;

    PlayerMessage parsedFromMessage = null;
    final Component component = packet.getUnsignedContent();

    for (SystemChatParser parser : systemChatParsers) {
      parsedFromMessage = parser.parse(component);
      if (parsedFromMessage != null) break;
    }

    if (parsedFromMessage == null) return;

    final PlayerMessage playerMessage = new PlayerMessage(sender, parsedFromMessage.contents(), "minecraft:chat", packet.getName()); // TODO: Fix chatType

    for (Listener listener : this.listeners) {
      listener.playerMessageReceived(playerMessage);
    }
  }

  public void packetReceived (Session session, ClientboundDisguisedChatPacket packet) {
    PlayerMessage parsedFromMessage = null;
    final Component component = packet.getMessage();

    for (SystemChatParser parser : systemChatParsers) {
      parsedFromMessage = parser.parse(component);
      if (parsedFromMessage != null) break;
    }

    if (parsedFromMessage == null) return;

    final PlayerMessage playerMessage = new PlayerMessage(parsedFromMessage.sender(), parsedFromMessage.contents(), "minecraft:chat", packet.getName()); // TODO: Fix chatType

    for (Listener listener : this.listeners) {
      listener.playerMessageReceived(playerMessage);
    }
  }

  // ? Should this be here?
  // TODO: Break up the method to make it less messy
  public void tellraw (Component message, String targets) {
    final int maxLength = client.core().maxCommandLength();

    final String raw = GsonComponentSerializer.gson().serialize(message);
    String command = "minecraft:tellraw " + targets + " " + raw;

    if (command.length() > maxLength) {
      String tagString;
      boolean interpret;

      if (message instanceof TextComponent && message.style().isEmpty() && (message.children() == null || message.children().size() == 0)) {
        tagString = ((TextComponent) message).content();
        interpret = false;
      } else {
        tagString = raw;
        interpret = true;
      }

      if (tagString.getBytes(StandardCharsets.UTF_8).length > 65535) return;

      final NbtMapBuilder blockEntityTagBuilder = NbtMap.builder();

      final NbtMap itemTag = blockEntityTagBuilder.putString("m", tagString).build();

      final Map<DataComponentType<?>, DataComponent<?, ?>> map = new HashMap<>();

      map.put(DataComponentType.BLOCK_ENTITY_DATA, DataComponentType.BLOCK_ENTITY_DATA.getDataComponentFactory().create(DataComponentType.BLOCK_ENTITY_DATA, itemTag));

      final DataComponents dataComponents = new DataComponents(map);

      final Session session = client.session();

      session.send(new ServerboundSetCreativeModeSlotPacket((short) 26, new ItemStack(1 /* stone */, 1, dataComponents)));
      client.core().run("minecraft:tellraw " + targets + " {\"nbt\":\"Inventory[0].tag.m\",\"entity\":\"" + client.profile().getIdAsString() + "\",\"interpret\":" + interpret + "}"); // TODO: Use GSON instead of concatenating strings, and hardcode less of this (it shouldn't matter here but yes)

      return;
    }

    client.core().run(command);
  }
  public void tellraw (Component message) { tellraw(message, "@a"); }
  public void tellraw (Component message, UUID uuid) { tellraw(message, UUIDUtilities.selector(uuid)); }

  public static class Listener {
    public void systemMessageReceived (Component component, boolean overlay) {}
    public void playerMessageReceived (PlayerMessage message) {}
  }

  public void addListener (Listener listener) { listeners.add(listener); }
  public void removeListener (Listener listener) { listeners.remove(listener); }
}
