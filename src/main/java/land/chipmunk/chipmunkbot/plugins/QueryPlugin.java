package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.Client;
import org.cloudburstmc.math.vector.Vector3i;
import org.cloudburstmc.nbt.NbtMap;
import org.geysermc.mcprotocollib.network.packet.Packet;
import org.geysermc.mcprotocollib.network.Session;
import org.geysermc.mcprotocollib.network.event.session.SessionListener;
import org.geysermc.mcprotocollib.network.event.session.SessionAdapter;
import org.geysermc.mcprotocollib.protocol.packet.ingame.serverbound.level.ServerboundBlockEntityTagQueryPacket;
import org.geysermc.mcprotocollib.protocol.packet.ingame.serverbound.level.ServerboundEntityTagQuery;
import org.geysermc.mcprotocollib.protocol.packet.ingame.clientbound.level.ClientboundTagQueryPacket;
import java.util.concurrent.CompletableFuture;
import java.util.Map;
import java.util.HashMap;

public class QueryPlugin extends SessionAdapter {
  private Client client;
  private int nextTransactionId = 0;
  private Map<Integer, CompletableFuture<NbtMap>> transactions = new HashMap<>();

  public QueryPlugin (Client client) {
    this.client = client;
    client.addListener((SessionListener) this);
  }

  public CompletableFuture<NbtMap> block (Vector3i position) {
    final int transactionId = nextTransactionId++;
    if (nextTransactionId > Integer.MAX_VALUE) nextTransactionId = 0; // ? Can and should I use negative numbers too?
    client.session().send(new ServerboundBlockEntityTagQueryPacket(transactionId, position));

    final CompletableFuture<NbtMap> future = new CompletableFuture<NbtMap>();
    transactions.put(transactionId, future);
    return future;
  }

  public CompletableFuture<NbtMap> entity (int entityId) {
    final int transactionId = nextTransactionId++;
    if (nextTransactionId > Integer.MAX_VALUE) nextTransactionId = 0; // ? Can and should I use negative numbers too?
    client.session().send(new ServerboundEntityTagQuery(transactionId, entityId));

    final CompletableFuture<NbtMap> future = new CompletableFuture<NbtMap>();
    transactions.put(transactionId, future);
    return future;
  }

  @Override
  public void packetReceived (Session session, Packet packet) {
    if (packet instanceof ClientboundTagQueryPacket) packetReceived(session, (ClientboundTagQueryPacket) packet);
  }

  public void packetReceived (Session session, ClientboundTagQueryPacket packet) {
    final CompletableFuture<NbtMap> future = transactions.get(packet.getTransactionId());
    if (future == null) return;
    future.complete(packet.getNbt());
    transactions.remove(future);
  }
}
