package land.chipmunk.chipmunkbot.plugins;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.Configuration;
import land.chipmunk.chipmunkbot.command.CommandSource;
import land.chipmunk.chipmunkbot.command.PlayerCommandSource;
import land.chipmunk.chipmunkbot.command.ComponentMessage;
import land.chipmunk.chipmunkbot.plugins.ChatPlugin;
import land.chipmunk.chipmunkbot.data.chat.PlayerMessage;
import land.chipmunk.chipmunkbot.util.ComponentUtilities;
import land.chipmunk.chipmunkbot.plugins.CommandManager;
import land.chipmunk.chipmunkbot.data.MutablePlayerListEntry;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.Message;
import lombok.Getter;
import lombok.Setter;

public class PlayerCommandHandler {
  private ChipmunkBot client;
  @Getter @Setter private String prefix;
  @Getter @Setter private String cspyPrefix;

  public PlayerCommandHandler (ChipmunkBot client, Configuration.Bot options) {
    this.client = client;
    this.prefix = options.commands.prefix;
    this.cspyPrefix = options.commands.cspyPrefix != null ? options.commands.cspyPrefix : this.prefix;

    // TODO: Make this less messy (I might just rewrite how I do events eventually)
    client.chat().addListener(new ChatPlugin.Listener() { @Override public void playerMessageReceived (PlayerMessage message) { handleMessage(message); } });
    client.commandSpy().addListener(new CommandSpyPlugin.Listener() { @Override public void commandReceived (MutablePlayerListEntry sender, String command, boolean senderHasCommandSpy) { handleCommand(sender, command); } });
  }

  public void handleMessage (PlayerMessage message) {
    final Component contents = message.contents();
    if (contents == null) return;
    final String contentsString = ComponentUtilities.stringify(contents);
    if (!contentsString.startsWith(prefix)) return;
    final String commandString = contentsString.substring(prefix.length());

    final PlayerCommandSource source = new PlayerCommandSource(client, message.sender());
    this.tryExecuteCommand(source, commandString);
  }

  public void handleCommand (MutablePlayerListEntry sender, String command) {
    if (!command.startsWith(cspyPrefix)) return;
    final String commandString = command.substring(cspyPrefix.length());

    final PlayerCommandSource source = new PlayerCommandSource(client, sender);
    this.tryExecuteCommand(source, commandString);
  }

  private void tryExecuteCommand (CommandSource source, String command) {
    try {
      CommandDispatcher dispatcher = client.commandManager().dispatcher();
      dispatcher.execute(command, source);
    } catch (CommandSyntaxException exception) {
      CommandManager.sendException(source, exception);
    } catch (Exception exception) {
      exception.printStackTrace();
      source.sendOutput(Component.translatable("command.failed", NamedTextColor.RED), false);
    }
  }
}
