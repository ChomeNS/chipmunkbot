package land.chipmunk.chipmunkbot.command;

import land.chipmunk.chipmunkbot.data.MutablePlayerListEntry;
import land.chipmunk.chipmunkbot.util.UUIDUtilities;
import land.chipmunk.chipmunkbot.ChipmunkBot;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.format.TextDecoration;
import net.kyori.adventure.text.format.Style;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.key.Key;
import org.geysermc.mcprotocollib.auth.GameProfile;

import java.util.UUID;

public class PlayerCommandSource extends CommandSource {
  private final MutablePlayerListEntry player;

  public PlayerCommandSource (ChipmunkBot client, MutablePlayerListEntry entry) {
    super(client);
    player = entry;
  }

  @Override
  public void sendOutput (Component message, boolean broadcast) {
    final UUID uuid = player.profile().getId();

    client().chat().tellraw(message, uuid);
    if (broadcast) client().chat().tellraw(Component.translatable("chat.type.admin", displayName(), message).color(NamedTextColor.GRAY).decoration(TextDecoration.ITALIC, true), UUIDUtilities.exclusiveSelector(uuid));
  }

  @Override
  public Component displayName () {
    final GameProfile profile = player.profile();
    final String username = profile.getName();
    final UUID uuid = profile.getId();

    final Style style = Style.style()
      .insertion(username)
      .clickEvent(ClickEvent.suggestCommand("/tell " + username + " "))
      .hoverEvent(HoverEvent.showEntity(Key.key("minecraft", "player"), uuid, Component.text(username)))
      .build();

    return Component.text(username, style);
  }

  @Override public MutablePlayerListEntry player () { return this.player; }
}
