package land.chipmunk.chipmunkbot;

import lombok.Getter;

public class Configuration {
  @Getter public Color color = new Color();

  public Bot[] bots = new Bot[]{};

  public static class Color {
    @Getter public String primary = "#ffc0cb";
    @Getter public String secondary = "#c95367";
  }

  public static class Bot {
    public String host = "0.0.0.0";
    public int port = 25565;
    public String username = "Player";
    // public ProxyInfo proxy;
    public long reconnectDelay = 1000;

    public Commands commands = new Commands();

    public Core core = new Core();

    public SelfCares selfCares = new SelfCares();

    public static class SelfCares {
      public boolean cspy = true;
      public PrefixSelfCare prefix = new PrefixSelfCare();

      public static class PrefixSelfCare {
        public boolean enabled = true;
        public String prefix = "&x&c&9&5&3&6&7[&x&f&f&c&0&c&bPrefix: s'&x&c&9&5&3&6&7]";
      }
    }

    public static class Commands {
      public String prefix = "default.";
      public String cspyPrefix = "default.";
    }

    public static class Core {
      public boolean enabled = true;
    }
  }
}
