package land.chipmunk.chipmunkbot.song;

import land.chipmunk.chipmunkbot.plugins.SongPlayer;
import land.chipmunk.chipmunkbot.util.DownloadUtilities;
import net.kyori.adventure.text.Component;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SongLoaderThread extends Thread {
  private String location;
  private File songPath;
  private URL songUrl;
  public SongLoaderException exception;
  public Song song;

  private boolean isUrl = false;

  public SongLoaderThread (URL location) throws SongLoaderException {
    isUrl = true;
    songUrl = location;
  }

  public SongLoaderThread (Path location) throws SongLoaderException {
    isUrl = false;
    songPath = location.toFile();
  }

  public void run () {
    byte[] bytes;
    String name;
    try {
      if (isUrl) {
        bytes = DownloadUtilities.DownloadToByteArray(songUrl, 10*1024*1024);
        name = Paths.get(songUrl.toURI().getPath()).getFileName().toString();
      } else {
        bytes = Files.readAllBytes(songPath.toPath());
        name = songPath.getName();
      }
    } catch (Exception e) {
      exception = new SongLoaderException(Component.text(e.getMessage()), e);
      return;
    }

    try {
      song = MidiConverter.getSongFromBytes(bytes, name);
    } catch (Exception e) {
    }

    if (song == null) {
      try {
        song = NBSConverter.getSongFromBytes(bytes, name);
      } catch (Exception e) {
      }
    }

    if (song == null) {
      exception = new SongLoaderException(Component.translatable("Invalid song format"));
    }
  }

  private File getSongFile (String name) {
    return new File(SongPlayer.SONG_DIR, name);
  }
}
