package land.chipmunk.chipmunkbot.systemChat;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.data.MutablePlayerListEntry;
import land.chipmunk.chipmunkbot.data.chat.PlayerMessage;
import land.chipmunk.chipmunkbot.data.chat.SystemChatParser;
import land.chipmunk.chipmunkbot.util.ComponentUtilities;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TranslatableComponent;
import net.kyori.adventure.text.event.HoverEvent;

import java.util.List;
import java.util.UUID;

// ported from chomens sex bot
public class U203aChatParser implements SystemChatParser {
    private final ChipmunkBot client;

    public U203aChatParser(ChipmunkBot client) {
        this.client = client;
    }

    @Override
    public PlayerMessage parse (Component message) {
        if (message instanceof TranslatableComponent) return parse((TranslatableComponent) message);
        return null;
    }

    // very similar to MinecraftChatParser
    public PlayerMessage parse (TranslatableComponent message) {
        final List<Component> args = message.args();
        if (args.size() < 3 || (!message.key().equals("[%s] %s › %s") && !message.key().equals("%s %s › %s"))) return null;

        final Component senderComponent = args.get(1);
        final Component contents = args.get(2);

        MutablePlayerListEntry sender;
        final HoverEvent<?> hoverEvent = senderComponent.hoverEvent();
        if (hoverEvent != null && hoverEvent.action().equals(HoverEvent.Action.SHOW_ENTITY)) {
            HoverEvent.ShowEntity entityInfo = (HoverEvent.ShowEntity) hoverEvent.value();
            final UUID senderUUID = entityInfo.id();
            sender = client.playerList().getEntry(senderUUID);
        } else {
            final String stringUsername = ComponentUtilities.stringify(senderComponent);
            sender = client.playerList().getEntry(stringUsername);
        }

        if (sender == null) return null;

        return new PlayerMessage(sender, contents, "minecraft:chat", senderComponent);
    }
}
