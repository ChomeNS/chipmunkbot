package land.chipmunk.chipmunkbot.systemChat;

import org.geysermc.mcprotocollib.auth.GameProfile;
import org.geysermc.mcprotocollib.protocol.data.game.entity.player.GameMode;
import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.data.MutablePlayerListEntry;
import land.chipmunk.chipmunkbot.data.chat.PlayerMessage;
import land.chipmunk.chipmunkbot.data.chat.SystemChatParser;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TranslatableComponent;
import net.kyori.adventure.text.event.HoverEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class MinecraftChatParser implements SystemChatParser {
  // ? Is such a mapping necessary?
  private static final Map<String, String> typeMap = new HashMap<>();
  static {
    typeMap.put("chat.type.text", "minecraft:chat");
    typeMap.put("chat.type.announcement", "minecraft:say_command");
    typeMap.put("chat.type.command", "minecraft:msg_command");
    typeMap.put("chat.type.team.text", "minecraft:team_msg_command");
    typeMap.put("chat.type.emote", "minecraft:emote_command");
  }

  private final ChipmunkBot client;

  public MinecraftChatParser (ChipmunkBot client) {
    this.client = client;
  }

  @Override
  public PlayerMessage parse (Component message) {
    if (message instanceof TranslatableComponent) return parse((TranslatableComponent) message);
    return null;
  }

  public PlayerMessage parse (TranslatableComponent message) {
    final List<Component> args = message.args();
    final String key = message.key();
    if (args == null || args.size() < 2 || !typeMap.containsKey(key)) return null;

    final String type = typeMap.get(key);

    final Component senderComponent = args.get(0);
    final Component contents = args.get(1);

    // Find the sender and attempt to map it to a player    
    final HoverEvent<?> hoverEvent = senderComponent.hoverEvent();
    if (hoverEvent == null || !hoverEvent.action().equals(HoverEvent.Action.SHOW_ENTITY)) return null;
    HoverEvent.ShowEntity entityInfo = (HoverEvent.ShowEntity) hoverEvent.value();
    final UUID senderUUID = entityInfo.id();

    MutablePlayerListEntry sender = client.playerList().getEntry(senderUUID);
    if (sender == null) sender = new MutablePlayerListEntry(new GameProfile(senderUUID, null), GameMode.SURVIVAL, true, 0, entityInfo.name(), 0L, null, new byte[0]);

    return new PlayerMessage(sender, contents, "minecraft:chat", senderComponent);
  }
}
