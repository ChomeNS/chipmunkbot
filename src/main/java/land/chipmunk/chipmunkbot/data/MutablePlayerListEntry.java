package land.chipmunk.chipmunkbot.data;

import org.geysermc.mcprotocollib.auth.GameProfile;
import org.geysermc.mcprotocollib.protocol.data.game.PlayerListEntry;
import org.geysermc.mcprotocollib.protocol.data.game.entity.player.GameMode;
import net.kyori.adventure.text.Component;
import lombok.Data;
import lombok.AllArgsConstructor;

import java.security.PublicKey;

@Data
@AllArgsConstructor
public class MutablePlayerListEntry {
  private GameProfile profile;
  private GameMode gamemode;
  private boolean listed;
  private int latency;
  private Component displayName;
  private long expiresAt;
  private PublicKey publicKey;
  private byte[] keySignature;

  public MutablePlayerListEntry (PlayerListEntry entry) {
    this(entry.getProfile(), entry.getGameMode(), entry.isListed(), entry.getLatency(), entry.getDisplayName(), entry.getExpiresAt(), entry.getPublicKey(), entry.getKeySignature());
  }
}
