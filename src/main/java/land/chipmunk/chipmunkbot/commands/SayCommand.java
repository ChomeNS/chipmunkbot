package land.chipmunk.chipmunkbot.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.command.CommandSource;
import land.chipmunk.chipmunkbot.command.ComponentMessage;
import net.kyori.adventure.text.Component;

import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.argument;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.literal;

public class SayCommand {
    public static void register (CommandDispatcher dispatcher) {
        final SayCommand instance = new SayCommand();

        dispatcher.register(
                literal("say")
                        .then(
                                argument("message", greedyString())
                                        .executes(instance::say)
                        )
        );
    }

    public int say (CommandContext<CommandSource> context) {
        final CommandSource source = context.getSource();
        final String message = getString(context, "message");

        final ChipmunkBot client = source.client();

        if (message.startsWith("/")) client.chat().command(message.substring(1));
        else client.chat().message(message);

        return 1;
    }
}
