package land.chipmunk.chipmunkbot.commands;

import com.mojang.brigadier.Message;
import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.Configuration;
import land.chipmunk.chipmunkbot.command.*;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.literal;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import com.mojang.brigadier.tree.CommandNode;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.text.JoinConfiguration;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;

import java.util.List;
import java.util.ArrayList;

public class HelpCommand {
  public static void register (CommandDispatcher dispatcher) {
    final HelpCommand instance = new HelpCommand();

    dispatcher.register(
      literal("help")
              .executes(instance::sendCommandList)
              .then(
                argument("command", greedyString())
                  .executes(instance::sendUsage)
              )
    );
  }

  public int sendCommandList (CommandContext<CommandSource> context) {
    final CommandSource source = context.getSource();
    final ChipmunkBot client = source.client();

    final CommandDispatcher<CommandSource> dispatcher = client.commandManager().dispatcher();

    source.sendOutput(generateCommandList(client, dispatcher), false);

    return 1;
  }

  public int sendUsage (CommandContext<CommandSource> context) throws CommandSyntaxException {
    final CommandSource source = context.getSource();
    final ChipmunkBot client = source.client();

    final String commandName = getString(context, "command");

    final CommandDispatcher<CommandSource> dispatcher = client.commandManager().dispatcher();
    for (CommandNode<CommandSource> node : dispatcher.getRoot().getChildren()) {
      if (!node.getName().equals(commandName)) continue;
      source.sendOutput(generateUsages(client.config(), dispatcher, node), false);

      return 1;
    }

    throw CommandSyntaxException.BUILT_IN_EXCEPTIONS.dispatcherUnknownCommand().create();
  }

  public Component generateCommandList (ChipmunkBot client, CommandDispatcher<CommandSource> dispatcher) {
    final List<Component> list = new ArrayList<>();

    for (CommandNode<CommandSource> node : dispatcher.getRoot().getChildren()) {
      final String name = node.getName();

      final Component usages = generateUsages(client.config(), dispatcher, node);
      final HoverEvent hoverEvent = HoverEvent.showText(usages);

      list.add(
              Component
                      .text(name)
                      .color(TextColor.fromHexString(client.config().color().primary()))
                      .hoverEvent(hoverEvent)
      );
    }

    return Component.translatable(
                    "%s - %s",
                    Component.text("Commands").color(NamedTextColor.GRAY),
                    Component.join(JoinConfiguration.separator(Component.space()), list)
            ).color(NamedTextColor.DARK_GRAY);
  }

  public Component generateUsages (Configuration config, CommandDispatcher dispatcher, CommandNode<CommandSource> node) {
    final List<Component> components = new ArrayList<>();
    final List<Component> usages = new ArrayList<>();

    for (String usage : dispatcher.getAllUsage(node, null, true)) {
      usages.add(
              Component
                      .text(node.getName())
                      .color(TextColor.fromHexString(config.color().secondary()))
                      .append(Component.space())
                      .append(
                              Component
                                      .text(usage)
                                      .color(TextColor.fromHexString(config.color().primary()))
                      )
      );
    }

    components.add(
            Component.translatable(
                    "%s",
                    Component.text(node.getName()).color(TextColor.fromHexString(config.color().primary()))
            ).color(NamedTextColor.DARK_GRAY)
    );

    components.add(Component.join(JoinConfiguration.separator(Component.newline()), usages));

    return Component.join(JoinConfiguration.separator(Component.newline()), components);
  }
}
