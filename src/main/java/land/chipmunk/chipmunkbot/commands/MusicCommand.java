package land.chipmunk.chipmunkbot.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.command.CommandSource;
import land.chipmunk.chipmunkbot.command.ComponentMessage;
import land.chipmunk.chipmunkbot.plugins.SongPlayer;
import land.chipmunk.chipmunkbot.song.Song;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.JoinConfiguration;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.mojang.brigadier.arguments.FloatArgumentType.getFloat;
import static com.mojang.brigadier.arguments.IntegerArgumentType.getInteger;
import static com.mojang.brigadier.arguments.IntegerArgumentType.integer;
import static com.mojang.brigadier.arguments.FloatArgumentType.floatArg;
import static com.mojang.brigadier.arguments.LongArgumentType.getLong;
import static land.chipmunk.chipmunkbot.command.arguments.LocationArgumentType.*;
import static land.chipmunk.chipmunkbot.command.arguments.TimestampArgumentType.timestamp;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.argument;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.literal;

public class MusicCommand {
  private static SimpleCommandExceptionType NO_SONG_IS_CURRENTLY_PLAYING = new SimpleCommandExceptionType(ComponentMessage.wrap(Component.translatable("No song is currently playing")));
  private static SimpleCommandExceptionType OOB_TIMESTAMP = new SimpleCommandExceptionType(ComponentMessage.wrap(Component.translatable("Invalid timestamp for the current song")));
  private static SimpleCommandExceptionType DIRECTORY_DOES_NOT_EXIST = new SimpleCommandExceptionType(ComponentMessage.wrap(Component.translatable("The specified directory does not exist")));

  public static void register (CommandDispatcher dispatcher) {
    final MusicCommand instance = new MusicCommand();

    Path root = Path.of(SongPlayer.SONG_DIR.getPath());

    dispatcher.register(
      literal("music")
        .then(
          literal("play")
            .then(
              argument("location", location(root))
                .executes(instance::play)
            )
        )

        .then(literal("stop").executes(instance::stop))
        .then(literal("skip").executes(instance::skip))
        .then(literal("pause").executes(instance::pause))

        .then(
          literal("list")
            .executes(c -> instance.list(c, root))
              .then(
                argument("location", filepath(root))
                  .executes(c -> instance.list(c, getPath(c, "location")))
              )
        )

        .then(
          literal("loop")
            .executes(instance::toggleLoop)
            .then(
              argument("count", integer())
                .executes(instance::loop)
            )
        )

        .then(
          literal("goto")
            .then(
              argument("timestamp", timestamp())
                .executes(instance::gotoCommand)
            )
        )

        .then(
                literal("pitch")
                        .then(
                                argument("pitch", floatArg())
                                        .executes(instance::pitch)
                        )
        )
    );
  }

  public int play (CommandContext<CommandSource> context) {
    final CommandSource source = context.getSource();

    final SongPlayer songPlayer = source.client().songPlayer();

    final Path path = getPath(context, "location");

    if (path != null) songPlayer.loadSong(path);
    else songPlayer.loadSong(getUrl(context, "location"));

    return 1;
  }

  public int stop (CommandContext<CommandSource> context) throws CommandSyntaxException {
    final CommandSource source = context.getSource();
    final ChipmunkBot client = source.client();
    final SongPlayer songPlayer = client.songPlayer();

    if (songPlayer.currentSong() == null) throw NO_SONG_IS_CURRENTLY_PLAYING.create();

    songPlayer.stopPlaying();
    songPlayer.songQueue().clear();
    source.sendOutput(Component.translatable("Stopped music playback", TextColor.fromHexString(client.config().color().primary())));

    return 1;
  }

  public int skip (CommandContext<CommandSource> context) throws CommandSyntaxException {
    final CommandSource source = context.getSource();
    final ChipmunkBot client = source.client();
    final SongPlayer songPlayer = client.songPlayer();

    if (songPlayer.currentSong() == null) throw NO_SONG_IS_CURRENTLY_PLAYING.create();

    songPlayer.stopPlaying();
    source.sendOutput(Component.translatable("Skipped the current song", TextColor.fromHexString(client.config().color().primary())));

    return 1;
  }

  public int pause (CommandContext<CommandSource> context) throws CommandSyntaxException {
    final CommandSource source = context.getSource();
    final SongPlayer songPlayer = source.client().songPlayer();
    final Song currentSong = songPlayer.currentSong();

    if (currentSong == null) throw NO_SONG_IS_CURRENTLY_PLAYING.create();

    if (!currentSong.paused) {
      currentSong.pause();
      source.sendOutput(Component.translatable("Paused the current song"));
    } else {
      currentSong.play();
      source.sendOutput(Component.translatable("Unpaused the current song"));
    }

    return 1;
  }

  public int list (CommandContext<CommandSource> context, Path path) throws CommandSyntaxException {
    final CommandSource source = context.getSource();
    final ChipmunkBot client = source.client();
    final String prefix = client.playerCommandHandler().prefix();

    final File directory = path.toFile();
    final String[] filenames = directory.list();
    if (filenames == null) throw DIRECTORY_DOES_NOT_EXIST.create();

    final Path root = Path.of(SongPlayer.SONG_DIR.getAbsoluteFile().getPath()).toAbsolutePath();
    String relativePath;
    if (path.getNameCount() - root.getNameCount() > 0) relativePath = path.subpath(root.getNameCount(), path.getNameCount()).toString();
    else relativePath = "";

    final List<Component> directories = new ArrayList<>();
    final List<Component> files = new ArrayList<>();
    int i = 0;

    for (String filename : filenames) {
      final File file = new File(directory, filename);
      if (!file.isDirectory()) continue;

      final TextColor color = (i++ & 1) == 0 ? TextColor.fromHexString(client.config().color().secondary()) : TextColor.fromHexString(client.config().color().primary());

      final Path relativeFilepath = Path.of(relativePath, filename);
      final String escapedPath = escapePath(relativeFilepath.toString());

      directories.add(
        Component.text(filename + "/", color)
          .clickEvent(ClickEvent.suggestCommand(prefix + "music list " + escapedPath))
          .hoverEvent(HoverEvent.showText(Component.translatable("Click to list %s", Component.text(filename))))
      );
    }

    for (String filename : filenames) {
      final File file = new File(directory, filename);
      if (file.isDirectory()) continue;

      final TextColor color = (i++ & 1) == 0 ? TextColor.fromHexString(client.config().color().secondary()) : TextColor.fromHexString(client.config().color().primary());

      final Path relativeFilepath = Path.of(relativePath, filename);
      final String escapedPath = escapePath(relativeFilepath.toString());

      files.add(
        Component.text(filename, color)
          .clickEvent(ClickEvent.suggestCommand(prefix + "music play " + escapedPath))
          .hoverEvent(HoverEvent.showText(Component.translatable("Click to play %s", Component.text(filename))))
      );
    }

    final ArrayList<Component> mergedList = new ArrayList<>();
    for (Component component : directories) mergedList.add(component);
    for (Component component : files) mergedList.add(component);
    final Component component = Component.translatable("Songs - %s", Component.join(JoinConfiguration.separator(Component.space()), mergedList)).color(TextColor.fromHexString(client.config().color().primary()));
    source.sendOutput(component, false);

    return 1;
  }

  // TODO: Move this into some utility class, as it is more related to brigadier strings in general than to the list command in specific
  private String escapePath (String path) {
    final StringBuilder sb = new StringBuilder("'");

    for (char character : path.toCharArray()) {
      if (character == '\'' || character == '\\') sb.append('\\');
      sb.append(character);
    }

    sb.append("'");
    return sb.toString();
  }

  public int toggleLoop (CommandContext<CommandSource> context) throws CommandSyntaxException {
    final CommandSource source = context.getSource();
    final SongPlayer songPlayer = source.client().songPlayer();
    final Song currentSong = songPlayer.currentSong();

    if (currentSong == null) throw NO_SONG_IS_CURRENTLY_PLAYING.create();

    currentSong.looping = !currentSong.looping;

    source.sendOutput(Component.translatable(currentSong.looping ? "Enabled looping" : "Disabled looping"));

    return 1;
  }


  public int loop (CommandContext<CommandSource> context) throws CommandSyntaxException {
    final CommandSource source = context.getSource();
    final SongPlayer songPlayer = source.client().songPlayer();
    final Song currentSong = songPlayer.currentSong();
    final int count = getInteger(context, "count");

    if (currentSong == null) throw NO_SONG_IS_CURRENTLY_PLAYING.create();

    currentSong.looping = true;
    currentSong.loopCount = count;

    source.sendOutput(Component.translatable("Enabled looping for %s times", Component.text(count)));

    return 1;
  }

  public int gotoCommand (CommandContext<CommandSource> context) throws CommandSyntaxException {
    final CommandSource source = context.getSource();
    final SongPlayer songPlayer = source.client().songPlayer();
    final Song currentSong = songPlayer.currentSong();
    final long millis = getLong(context, "timestamp");

    if (currentSong == null) throw NO_SONG_IS_CURRENTLY_PLAYING.create();

    if (millis < 0 || millis > currentSong.length) throw OOB_TIMESTAMP.create();

    currentSong.setTime(millis);

    source.sendOutput(Component.translatable("Set the current time of the song to %s", songPlayer.formatTime(millis)));

    return 1;
  }

  public int pitch (CommandContext<CommandSource> context) {
    final CommandSource source = context.getSource();
    final SongPlayer songPlayer = source.client().songPlayer();

    final float pitch = getFloat(context, "pitch");

    songPlayer.pitch(pitch);

    source.sendOutput(
            Component.translatable(
                    "Set the pitch to: %s",
                    Component.text(String.valueOf(pitch))
            ).color(TextColor.fromHexString(source.client().config().color().primary()))
    );

    return 1;
  }
}
