package land.chipmunk.chipmunkbot.commands;

import land.chipmunk.chipmunkbot.ChipmunkBot;
import land.chipmunk.chipmunkbot.util.CPUInfo;
import land.chipmunk.chipmunkbot.command.*;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.literal;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.event.ClickEvent;

import java.net.InetAddress;
import java.lang.management.*;
import java.io.IOException;

public class InfoCommand {
  private static final String ORIGINAL_REPOSITORY_URL = "https://code.chipmunk.land/ChipmunkMC/chipmunkbot";
  private static final String FORK_URL = "https://code.chipmunk.land/ChomeNS/chipmunkbot";

  private static ChipmunkBot client;

  public static void register (CommandDispatcher dispatcher) {
    final InfoCommand instance = new InfoCommand();

    dispatcher.register(
      literal("info")
        .executes(instance::sendBotInfo)
        .then(
          literal("server")
            .executes(instance::sendServerInfo)
        )
    );
  }

  public int sendBotInfo (CommandContext<CommandSource> context) {
    final CommandSource source = context.getSource();
    client = source.client();

    final Component component = Component.empty()
      .append(Component.text("ChipmunkBot", TextColor.fromHexString(client.config().color().primary())))
      .append(Component.text(" - A utility bot for free-operator servers with minimal or no restrictions", NamedTextColor.GRAY))
      .append(Component.newline())
      .append(Component.text("Made by ", NamedTextColor.GRAY))
      .append(Component.text("_ChipMC_", TextColor.fromHexString(client.config().color().secondary())))
      .append(Component.text(", ", TextColor.fromHexString(client.config().color().primary())))
      .append(Component.text("chayapak", TextColor.fromHexString(client.config().color().secondary())))
      .append(Component.text(" (").color(NamedTextColor.DARK_GRAY))
      .append(
              Component
                      .text(":3")
                      .color(NamedTextColor.RED)
      )
      .append(Component.text(")").color(NamedTextColor.DARK_GRAY))
      .append(Component.text(" and contributors", NamedTextColor.GRAY))
      .append(Component.newline())
      .append(Component.text("Original Repository: ", NamedTextColor.GRAY))
      .append(Component.text(ORIGINAL_REPOSITORY_URL, TextColor.fromHexString(client.config().color().primary())).clickEvent(ClickEvent.openUrl(ORIGINAL_REPOSITORY_URL)))
      .append(Component.newline())
      .append(Component.text("My fork: ", NamedTextColor.GRAY))
      .append(Component.text(FORK_URL, TextColor.fromHexString(client.config().color().primary())).clickEvent(ClickEvent.openUrl(FORK_URL)));

    source.sendOutput(component, false);

    return 1;
  }

  public int sendServerInfo (CommandContext<CommandSource> context) {
    final CommandSource source = context.getSource();

    client = source.client();

    final Runtime runtime = Runtime.getRuntime();
    final OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
    final RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
    final MemoryMXBean memory = ManagementFactory.getMemoryMXBean();

    // ? Should I send a single message that uses newlines instead?
    source.sendOutput(formatEntry(Component.translatable("Working directory"), Component.text(System.getProperty("user.dir"))), false);
    try {
      final InetAddress localhost = InetAddress.getLocalHost();
      source.sendOutput(formatEntry(Component.translatable("Hostname"), Component.text(localhost.getHostName())), false);
      source.sendOutput(formatEntry(Component.translatable("IP address"), Component.text(localhost.getHostAddress())), false);
    } catch (Exception ignored) {
    }

    source.sendOutput(formatEntry(Component.translatable("OS Name"), Component.text(os.getName())), false);
    source.sendOutput(formatEntry(Component.translatable("OS version"), Component.text(os.getVersion())), false);
    source.sendOutput(formatEntry(Component.translatable("OS architecture"), Component.text(os.getArch())), false);
    source.sendOutput(formatEntry(Component.translatable("CPU cores"), Component.text(os.getAvailableProcessors())), false);
    source.sendOutput(formatEntry(Component.translatable("CPU load"), Component.text(os.getSystemLoadAverage())), false);

    try {
      final String uname = new String(runtime.exec(new String[] {"uname", "-a"}).getInputStream().readAllBytes()).trim();
      source.sendOutput(formatEntry(Component.translatable("uname -a"), Component.text(uname)), false);
    } catch (IOException ignored) {
    }

    try {
      final String cpuModel = CPUInfo.readCpuInfo().get("model name");
      source.sendOutput(formatEntry(Component.translatable("CPU model"), Component.text(cpuModel)), false);
    } catch (IOException ignored) {
    }

    source.sendOutput(formatEntry(Component.translatable("Java VM"), Component.text(runtimeMXBean.getVmName())), false);
    source.sendOutput(formatEntry(Component.translatable("Java version"), Component.text(runtimeMXBean.getVmVersion())), false);

    source.sendOutput(formatEntry(Component.translatable("Heap memory usage"), formatMemoryUsage(memory.getHeapMemoryUsage())), false);

    return 1;
  }

  public Component formatEntry (Component key, Component value) {
    return Component.translatable(
      "%s: %s",
      key.color(TextColor.fromHexString(client.config().color().primary())),
      value.color(TextColor.fromHexString(client.config().color().secondary()))
    );
  }

  public Component formatMemoryUsage (long used, long max) {
    return Component.translatable(
      "%sMB/%sMB",
      Component.text(used / 1024l / 1024l),
      Component.text(max / 1024l / 1024l)
    );
  }

  public Component formatMemoryUsage (MemoryUsage usage) {
    return formatMemoryUsage(usage.getUsed(), usage.getMax());
  }
}
