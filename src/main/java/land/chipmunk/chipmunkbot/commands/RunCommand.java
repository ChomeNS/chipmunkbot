package land.chipmunk.chipmunkbot.commands;

import land.chipmunk.chipmunkbot.ChipmunkBot; 
import land.chipmunk.chipmunkbot.command.*;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.literal;
import static land.chipmunk.chipmunkbot.plugins.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import org.cloudburstmc.nbt.NbtMap;

import java.util.concurrent.CompletableFuture;

public class RunCommand {
  public static void register (CommandDispatcher dispatcher) {
    final RunCommand instance = new RunCommand();

    dispatcher.register(
      literal("run")
              .then(
                argument("command", greedyString())
                  .executes(instance::run)
              )
    );
  }

  public int run (CommandContext<CommandSource> context) {
    final CommandSource source = context.getSource();
    final ChipmunkBot client = source.client();

    final CompletableFuture<NbtMap> future = client.core().runTracked(getString(context, "command"));

    future.thenApply(tag -> {
      if (!tag.containsKey("LastOutput") || !(tag.get("LastOutput") instanceof String)) return tag;
      final String outputJson = tag.getString("LastOutput");

      try {
        final Component output = GsonComponentSerializer.gson().deserialize(outputJson);
        source.sendOutput(output);
      } catch (Exception exception) {
      }

      return tag;
    });

    return 1;
  }
}
