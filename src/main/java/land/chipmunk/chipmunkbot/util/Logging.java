package land.chipmunk.chipmunkbot.util;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

public interface Logging {
  static File LOGS_DIR = new File("logs");
  static File LATEST_LOG_FILE = new File(LOGS_DIR, "latest.log");

  static InputStream getInputStream (InputStream is) throws IOException {
    try {
      return new GZIPInputStream(is);
    } catch (IOException ignored) {
      is.reset();
      return is;
    }
  }

  static InputStream getInputStream (File file) throws IOException {
    return getInputStream(new FileInputStream(file));
  }
}
