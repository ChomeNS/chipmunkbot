package land.chipmunk.chipmunkbot.util;

public class MathUtilities {
    public static double clamp (double value, double min, double max) {
        return Math.max(Math.min(value, max), min);
    }
}
